/**
 *
 */
package org.security.userdetails;

import org.springframework.security.GrantedAuthority;
import org.springframework.security.userdetails.UserDetails;

/**
 * @author cui
 *
 */
public interface MyUserDetails extends UserDetails {

	Integer getUserId();

	void setUserId(Integer userId);

	String getEmail();

	void setEmail(String email);

	String getUsername();

	void setUsername(String username);

	GrantedAuthority[] getAuthorities();

	void setAuthorities(GrantedAuthority[] authorities);

}
