/**
 *
 */
package org.security.userdetails;

import org.springframework.security.GrantedAuthority;
import org.springframework.security.userdetails.User;

/**
 * @author cui
 *
 */
public class MyUser extends User implements MyUserDetails {

	/**
	 *
	 */
	private static final long serialVersionUID = 5157710602117592859L;
	private Integer userId;
	private String email;
	private String username;
	private GrantedAuthority[] authorities;

	public MyUser(String username, String password,
			boolean enabled, boolean accountNonExpired,
			boolean credentialsNonExpired, boolean accountNonLocked,
			GrantedAuthority[] authorities) throws IllegalArgumentException {
		super(username, password, enabled, accountNonExpired,
				credentialsNonExpired, accountNonLocked, authorities);
		setUsername(username);
		setAuthorities(authorities);
	}

	public MyUser(Integer userId, String username, String password,
			boolean enabled, String email, boolean accountNonExpired,
			boolean credentialsNonExpired, boolean accountNonLocked,
			GrantedAuthority[] authorities) throws IllegalArgumentException {
		super(username, password, enabled, accountNonExpired,
				credentialsNonExpired, accountNonLocked, authorities);
		this.userId = userId;
		this.email = email;
		setUsername(username);
		setAuthorities(authorities);
	}

	/* (non-Javadoc)
	 * @see org.security.userdetails.MyUserDetails#getEmail()
	 */
	public String getEmail() {
		// TODO Auto-generated method stub
		return (this.email);
	}

	/* (non-Javadoc)
	 * @see org.security.userdetails.MyUserDetails#setEmail(java.lang.String)
	 */
	public void setEmail(String email) {
		// TODO Auto-generated method stub
		this.email = email;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.security.userdetails.MyUserDetails#getUserId()
	 */
	public Integer getUserId() {
		// TODO Auto-generated method stub
		return (this.userId);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.security.userdetails.MyUserDetails#setUserId(java.lang.Integer)
	 */
	public void setUserId(Integer userId) {
		// TODO Auto-generated method stub
		this.userId = userId;
	}

	/**
	 * @return the userName
	 */

	/* (non-Javadoc)
	 * @see org.security.userdetails.MyUserDetails#setUsername(java.lang.String)
	 */
	public void setUsername(String username) {
		// TODO Auto-generated method stub
		this.username = username;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @return the authorities
	 */
	public GrantedAuthority[] getAuthorities() {
		return authorities;
	}

	/**
	 * @param authorities the authorities to set
	 */
	public void setAuthorities(GrantedAuthority[] authorities) {
		this.authorities = authorities;
	}


}
