/**
 *
 */
package org.security.userdetails;

import org.springframework.security.Authentication;
import org.springframework.security.context.SecurityContext;
import org.springframework.security.context.SecurityContextHolder;

/**
 * @author Administrator
 *
 */
public class CurrentUser {

	public static MyUserDetails getUser() {

		SecurityContext context = SecurityContextHolder.getContext();
		if ( context == null
			|| !(context instanceof SecurityContext)
			|| context.getAuthentication() == null) {

			return null;
		}
		Authentication authentication = context.getAuthentication();
		if (authentication.getPrincipal() == null) {
			return null;
		}
		MyUserDetails user = null;
		if (authentication.getPrincipal() instanceof MyUserDetails) {
			user = (MyUserDetails)authentication.getPrincipal();
		}
		return user;
	}

}
