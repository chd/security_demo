/**
 *
 */
package org.security.userdetails.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.List;

import javax.sql.DataSource;

import org.security.userdetails.MyUser;
import org.security.userdetails.MyUserDetails;
import org.springframework.context.ApplicationContextException;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.object.MappingSqlQuery;
import org.springframework.security.GrantedAuthority;
import org.springframework.security.GrantedAuthorityImpl;
import org.springframework.security.userdetails.UserDetails;
import org.springframework.security.userdetails.UserDetailsService;
import org.springframework.security.userdetails.UsernameNotFoundException;

/**
 * @author 唐泰彬 E-mail:redhatlinux_root@yahoo.com.cn
 * @version 创建时间：2008 Aug 20, 2008 14:56:20 PM
 *
 */
public class MySecurityJdbcDaoImpl extends JdbcDaoSupport implements UserDetailsService {

	public static final String DEF_USERS_BY_USERNAME_QUERY =
		"SELECT userName, passWord, enabled, userId, email " +
		"FROM users " +
		"WHERE userName=?";
	public static final String DEF_AUTHORITIES_BY_USERNAME_QUERY =
		"SELECT u.userName, r.roleName  " +
		"FROM users u, roles r, users_roles ur " +
		"WHERE u.userId = ur.userId " +
			"AND r.roleId = ur.roleId " +
			"AND u.userName=?";

	protected MappingSqlQuery authoritiesByUsernameMapping;
	protected MappingSqlQuery usersByUsernameMapping;
    private String authoritiesByUsernameQuery;
    private String usersByUsernameQuery;
    private String rolePrefix = "";
    private boolean usernameBasedPrimaryKey = true;

    private boolean debug = false;

    public MySecurityJdbcDaoImpl() {
    	this.usersByUsernameQuery = DEF_USERS_BY_USERNAME_QUERY;
    	this.authoritiesByUsernameQuery = DEF_AUTHORITIES_BY_USERNAME_QUERY;
    }

    protected void initDao() throws ApplicationContextException {
        initMappingSqlQueries();
    }

    protected void initMappingSqlQueries() {
    	this.usersByUsernameMapping =
    				new UsersByUsernameMapping(getDataSource());
    	this.authoritiesByUsernameMapping =
    				new AuthoritiesByUsernameMapping(getDataSource());
    }

    @SuppressWarnings("unchecked")
	protected void addCustomAuthorities(String username, List authorities) {}

	/* (non-Javadoc)
	 * @see org.springframework.security.userdetails.jdbc.JdbcDaoImpl#loadUserByUsername(java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException, DataAccessException {
		if (debug) {
			// TODO Auto-generated method stub
			System.out.println("用户<" + username +">登陆");
		}
		List users = usersByUsernameMapping.execute(username);

		if (users.size() == 0) {
			throw new UsernameNotFoundException("User Not Found...");
		}
		MyUserDetails user = (MyUserDetails)users.get(0);

		if (debug) {
			System.out.println("用户基本信息<" + user.getUserId()+"><"+user.getUsername()+"><"+user.getPassword()+"><"+user.getEmail() +">");
		}

		//Set dbAuthsSet = new HashSet();
		//dbAuthsSet.addAll(authoritiesByUsernameMapping.execute(user.getUsername()));
		//List dbAuths = new ArrayList(dbAuthsSet);
		List dbAuths = authoritiesByUsernameMapping.execute(user.getUsername());

		if (debug) {
			System.out.println("<" + user.getUsername() + ">有<" +dbAuths.size() +">个权限");
		}
		if (dbAuths.size() == 0) {
			throw new UsernameNotFoundException("User Has no GrantedAuthority");
		}

		addCustomAuthorities(user.getUsername(), dbAuths);

		GrantedAuthority[] authorities = (GrantedAuthority[])dbAuths.toArray(new GrantedAuthority[dbAuths.size()]);

		user.setAuthorities(authorities);

		if (!usernameBasedPrimaryKey) {
			user.setUsername(username);
		}
		return user;
	}

	protected class AuthoritiesByUsernameMapping extends MappingSqlQuery {

		protected AuthoritiesByUsernameMapping(DataSource ds) {
			super(ds, authoritiesByUsernameQuery);
			declareParameter(new SqlParameter(Types.VARCHAR));
			compile();
		}
		/* (non-Javadoc)
		 * @see org.springframework.jdbc.object.MappingSqlQuery#mapRow(java.sql.ResultSet, int)
		 */
		protected Object mapRow(ResultSet rs, int rownum) throws SQLException {
			// TODO Auto-generated method stub
			String roleName = rolePrefix + rs.getString(2);
			GrantedAuthorityImpl authority = new GrantedAuthorityImpl(roleName);
			if (debug) {
				System.out.println("权限名：<" + roleName +">");
			}
			return authority;
		}
	}

	protected class UsersByUsernameMapping extends MappingSqlQuery {

		protected UsersByUsernameMapping(DataSource ds) {
			super(ds, usersByUsernameQuery);
			declareParameter(new SqlParameter(Types.VARCHAR));
			compile();
		}

		/* (non-Javadoc)
		 * @see org.springframework.jdbc.object.MappingSqlQuery#mapRow(java.sql.ResultSet, int)
		 */
		protected Object mapRow(ResultSet rs, int rownum) throws SQLException {
			// TODO Auto-generated method stub
			String userName = rs.getString(1);
			String passWord = rs.getString(2);
			boolean enabled = rs.getBoolean(3);
			Integer userId = rs.getInt(4);
			String email = rs.getString(5);
			MyUserDetails user = new MyUser(userName, passWord, enabled, true, true,true,
					new GrantedAuthority[]{new GrantedAuthorityImpl("HOLDER")});
			user.setEmail(email);
			user.setUserId(userId);
			return user;
		}

	}

	/**
	 * @return the authoritiesByUsernameQuery
	 */
	public String getAuthoritiesByUsernameQuery() {
		return authoritiesByUsernameQuery;
	}

	/**
	 * @param authoritiesByUsernameQuery the authoritiesByUsernameQuery to set
	 */
	public void setAuthoritiesByUsernameQuery(String authoritiesByUsernameQuery) {
		this.authoritiesByUsernameQuery = authoritiesByUsernameQuery;
	}

	/**
	 * @return the usersByUsernameQuery
	 */
	public String getUsersByUsernameQuery() {
		return usersByUsernameQuery;
	}

	/**
	 * @param usersByUsernameQuery the usersByUsernameQuery to set
	 */
	public void setUsersByUsernameQuery(String usersByUsernameQuery) {
		this.usersByUsernameQuery = usersByUsernameQuery;
	}

	/**
	 * @return the rolePrefix
	 */
	public String getRolePrefix() {
		return rolePrefix;
	}

	/**
	 * @param rolePrefix the rolePrefix to set
	 */
	public void setRolePrefix(String rolePrefix) {
		this.rolePrefix = rolePrefix;
	}

	/**
	 * @return the usernameBasedPrimaryKey
	 */
	public boolean isUsernameBasedPrimaryKey() {
		return usernameBasedPrimaryKey;
	}

	/**
	 * @param usernameBasedPrimaryKey the usernameBasedPrimaryKey to set
	 */
	public void setUsernameBasedPrimaryKey(boolean usernameBasedPrimaryKey) {
		this.usernameBasedPrimaryKey = usernameBasedPrimaryKey;
	}

	/**
	 * @return the debug
	 */
	public boolean isDebug() {
		return debug;
	}

	/**
	 * @param debug the debug to set
	 */
	public void setDebug(boolean debug) {
		this.debug = debug;
	}

}
