<%@ page contentType="text/html; charset=UTF-8"%>

<%@page import="org.security.userdetails.MyUserDetails"%>
<%@page import="org.springframework.security.GrantedAuthority"%>
<%@page import="org.security.userdetails.CurrentUser"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>User Page</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

  </head>

  <body>
    This is user JSP page. <br>
    <a href="../default.jsp">Home</a><br><br>

<%
    MyUserDetails details = CurrentUser.getUser();
    GrantedAuthority[] authority = details.getAuthorities();
%>
	<table border="1" cellspacing="0" cellpadding="0" style="border: 1px solid #000000; text-align: left">
	<tbody>
		<tr>
			<th>编号</th>
			<th>用户名</th>
			<th>email</th>
			<th>权限</th>
		</tr>
		<tr>
			<td><%= details.getUserId() %></td>
			<td><%= details.getUsername() %></td>
			<td><%= details.getEmail() %></td>
			<td>
			<%
			    for(int i=0; i<authority.length; i++) {
			    	out.println("|" + authority[i]);
			    }
			%>
			</td>
		</tr>
	</tbody>
	</table>
  </body>
</html>
