DROP DATABASE spring;
CREATE DATABASE spring;
USE spring;

DROP TABLE users_roles;
DROP TABLE users;
DROP TABLE roles;

CREATE TABLE users
(
    userId INT NOT NULL AUTO_INCREMENT,
    userName VARCHAR(20) NOT NULL,
    passWord VARCHAR(32) NOT NULL,
    enabled BIT NOT NULL,
    email VARCHAR(50) NOT NULL,
    CONSTRAINT pk_u PRIMARY KEY (userId)
);

INSERT INTO users (userName,passWord,enabled,email) VALUES('admin','21232f297a57a5a743894a0e4a801fc3',1,'admin@springside.org.cn');
INSERT INTO users (userName,passWord,enabled,email) VALUES('user1','24c9e15e52afc47c225b757e7bee1f9d',1,'user1@springside.org.cn');
INSERT INTO users (userName,passWord,enabled,email) VALUES('user2','7e58d63b60197ceb55a1c487989a3720',1,'user2@springside.org.cn');
INSERT INTO users (userName,passWord,enabled,email) VALUES('user3','92877af70a45fd6a2ed7fe81e1236b78',0,'user3@springside.org.cn');

CREATE TABLE roles
(
    roleId INT NOT NULL AUTO_INCREMENT,
    title VARCHAR(20) NOT NULL,
    roleName VARCHAR(20) NOT NULL,
    description VARCHAR(50) NOT NULL,
    CONSTRAINT pk_r PRIMARY KEY (roleId)
);

INSERT INTO roles (title,roleName,description) VALUES('管理员','ROLE_SUPERVISOR','超级用户,拥有所有权限');
INSERT INTO roles (title,roleName,description) VALUES('用户','ROLE_USER','普通用户,只能浏览用户');
INSERT INTO roles (title,roleName,description) VALUES('其他','ROLE_TELLER','其他用户');

CREATE TABLE users_roles
(
    userId INTEGER NOT NULL,
    roleId INTEGER NOT NULL,
    FOREIGN KEY (userId) REFERENCES users(userId),
    FOREIGN KEY (roleId) REFERENCES roles(roleId)
);

INSERT INTO users_roles VALUES(1,1);
INSERT INTO users_roles VALUES(1,2);
INSERT INTO users_roles VALUES(2,2);
INSERT INTO users_roles VALUES(2,3);
INSERT INTO users_roles VALUES(3,2);
INSERT INTO users_roles VALUES(4,2);



select userName,passWord,enabled,userId,email from users where enabled=1 and userName='user1';

select u.userName,r.roleName  from users u,roles r,users_roles ur where u.userId=ur.userId and r.roleId=ur.roleId and u.enabled=1 and u.userName='user1';


















