<%@ page contentType="text/html; charset=UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>登录页面</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
  </head>
  <body>
    <P>Valid users:</P>
	<P>username: <b>admin</b>, password: <b>admin</b>
	<P>username: <b>user1</b>, password: <b>user1</b>
	<p>username: <b>user2</b>, password: <b>user2</b>
	<p>username: <b>user3</b>, password: <b>user3</b> (disabled)</p>
	<c:if test="${not empty param.login_error}">
		登录失败，请重试。错误原因:<br/>
		<font color="red">
			<c:if test="${not empty SPRING_SECURITY_LAST_EXCEPTION}">
				<c:out value="${SPRING_SECURITY_LAST_EXCEPTION}"></c:out>
			</c:if>
		</font>
	</c:if>
	<form action="<c:url value="/j_spring_security_check"/>" method="post">
		<table>
			<tr>
				<td><label for="username">username:</label></td>
				<td><input type="text" id="username" name="j_username" value="<c:out value="${SPRING_SECURITY_LAST_USERNAME}"/>"/></td>
			</tr>
			<tr>
				<td><label for="password">password:</label></td>
				<td><input type="password" id="password" name="j_password" value=""/></td>
			</tr>
			<tr><td></td>
				<td><input type="checkbox" name="_spring_security_remember_me">两周内记住我</td>
			</tr>
			<tr><td colspan="2"><input type="submit" value="提交"/>
			<input type="reset" value="重置"/></td></tr>
		</table>
	</form>
  </body>
</html>
